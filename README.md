# pamac-tray-icon-plasma

Pamac tray icon for plasma users

https://gitlab.com/LordTermor/pamac-tray-icon-plasma

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/pamac/pamac-tray-icon-plasma.git
```

